/**
 * @file Defines all routes in the application.
 * Makes it possible to statically type request and return values.
 * Can be shared with a TYPESCRIPT client side application.
 */

/******************** HELPER TYPES */
import { LoginRequest, RefreshTokenRequest, Section, SignupRequest, SyncResponse, Token } from "./auth";
import { Organization, Paginated, Ticket } from "./index";

/**
 * Basic route interface.
 * Defines request and response nodes.
 */
export interface Route<Request, Response = Request> {
    /**
     * The type of the request.
     */
    request: Request;
    /**
     * The type of the expected response.
     */
    response: Response;
}

/**
 * Post requests dont require the ID inside a entity, because it does not exist.
 */
export type POST<T> = Omit<T, "id">;
/**
 * Get Request can provide anything.
 */
export type GET<T> = any;
/**
 * A put request is required to provide an id.
 */
export type PUT<T> = T & { id: number };
/**
 * A patch request is required to provide an id.
 * Only provides the information, which should get CHANGED.
 */
export type PATCH<T> = Partial<T> & { id: number };
/**
 * Only the id is required to delete an entity.
 */
export type DELETE<T> = { id: number };

/******************** HELPER END */

export interface RouteMap<T> {
    POST: Route<POST<T>, T>;
    PATCH: Route<PATCH<T>>;
    DELETE: Route<{ id: number }, DELETE<T>>;
    PUT: Route<PUT<T>>;
    GET: Route<GET<T>, T[]>;
    SHOW: Route<{ id: number }, T>;
}

export interface AuthRoutes {
    SIGNUP: Route<SignupRequest, Token>;
    SYNC: Route<any, SyncResponse>;
    REFRESH_TOKEN: Route<RefreshTokenRequest, Token>;
    LOGIN: Route<LoginRequest, Token>;
}

export interface OrganizationRoutes extends RouteMap<Organization> {
    PATCH: Route<PATCH<Organization & { sections: { value: string; real?: boolean; key: string }[] }>, SyncResponse>;
}

export interface TicketRoutes {
    POST: Route<
        POST<Omit<Ticket, "position" | "state" | "createdAt"> & { sectionId: number }>,
        Ticket & { section: Section }
    >;
    PATCH: Route<PATCH<Ticket>, Ticket>;
    DELETE: Route<{ id: number }, DELETE<Ticket>>;
    PUT: Route<PUT<Ticket>>;
    GET: Route<GET<Ticket>, GET<Paginated<Ticket & { section: Section }>>>;
    SHOW: Route<{ id: number }, Ticket>;
}
